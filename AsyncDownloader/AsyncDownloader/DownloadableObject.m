//
//  DownloadableObject.m
//  
//
//  Created by Dmitry Mozyrchuk on 4/29/16.
//
//

#import "DownloadableObject.h"

@implementation DownloadableObject


- (instancetype)initWithURL:(NSURL *)url filePath:(NSString *)filePath fileName:(NSString *)fileName {
    
    self = [super init];
    if (self) {
        self.downloadURL = url;
        self.filePath = filePath;
        self.fileName = fileName;
        self.state = DownloadableObjectDownloadPlanned;
    }
    return self;
    
}

@end
