//
//  AppDelegate.h
//  AsyncDownloader
//
//  Created by Dmitry Mozyrchuk on 27.04.16.
//  Copyright (c) 2016 Dmitry Mozyrchuk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

