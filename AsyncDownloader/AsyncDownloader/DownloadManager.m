//
//  DownloadManager.m
//  
//
//  Created by Dmitry Mozyrchuk on 4/29/16.
//
//

#import "DownloadManager.h"
#import "DownloadOperation.h"

@interface DownloadManager ()


- (NSMutableArray *)prepareDownloadArray:(NSInteger)count downloadURL:(NSURL *)url filePath:(NSString *)filePath fileName:(NSString *)fileName;
- (void)removeSuccessfullyDownloadedObject:(NSString *)filePath;

@end

@implementation DownloadManager

#pragma mark init methods

- (instancetype)initWithNumberofDownloadableItems:(NSInteger)totalDownloadsCount numberOfConcurrentDownloads:(NSInteger)maxConcurrentDownloads downloadURL:(NSURL *)url filePath:(NSString *)filePath fileName:(NSString *)fileName delegate:(id<DownloadManagerDelegate>)delegate {
    
    self = [super init];
    if (self) {
        self.downloadableObjectsArray = [self prepareDownloadArray:totalDownloadsCount downloadURL:url filePath:filePath fileName:fileName];
        self.downloadQueue = [[NSOperationQueue alloc] init];
        self.downloadQueue.maxConcurrentOperationCount = maxConcurrentDownloads;
        self.delegate = delegate;
    }
    return self;
    
}

#pragma mark - private methods

- (NSMutableArray *)prepareDownloadArray:(NSInteger)count downloadURL:(NSURL *)url filePath:(NSString *)filePath fileName:(NSString *)fileName{
    
    NSMutableArray *downloadArray = [NSMutableArray arrayWithCapacity:count];
    for (int i = 0; i < count; i++) {
        DownloadableObject *downloadableObject = [[DownloadableObject alloc] initWithURL:url filePath:[NSString stringWithFormat:@"%@_%d", filePath, i] fileName:[NSString stringWithFormat:@"%@_%d", fileName, i+1]];
        [downloadArray addObject:downloadableObject];
    }
    return downloadArray;
}

- (void)removeSuccessfullyDownloadedObject:(NSString *)filePath {
    
    NSFileManager *fm = [NSFileManager defaultManager];
    NSError *error;
    if ([fm fileExistsAtPath:filePath]) {
        [fm removeItemAtPath:filePath error:&error];
    }
    
}

#pragma mark - Public methods

- (void)startDownloading {
    
    for (DownloadableObject *object in self.downloadableObjectsArray) {
        __weak typeof(self) welf = self;
        DownloadOperation *operation = [[DownloadOperation alloc] initWithURL:object.downloadURL
                                                                 downloadPath:object.filePath
                                                                firstResponse:^(NSURLResponse *response) {
                                                                    object.state = DownloadableObjectDownloadStarted;
                                                                    object.totalFileLength = [response expectedContentLength];
                                                                    object.currentDownloadedLenght = 0;
                                                                    welf.numberOfDownloadingItems++;
                                                                    if ([welf.delegate respondsToSelector:@selector(downloadStateDidChangeForObject:)]) {
                                                                        [welf.delegate downloadStateDidChangeForObject:object];
                                                                    }
                                                                }
                                                                     progress:^(uint64_t receivedLength, uint64_t totalLength) {
                                                                         object.currentDownloadedLenght = receivedLength;
                                                                         if ([welf.delegate respondsToSelector:@selector(downloadStateDidChangeForObject:)]) {
                                                                             [welf.delegate downloadStateDidChangeForObject:object];
                                                                         }
                                                                     }
                                                                        error:^(NSError *error) {
                                                                            object.state = DownloadableObjectDownloadCancelled;
                                                                            welf.numberOfDownloadingItems--;
                                                                            welf.numberOfDownloadedItems++;
                                                                            if ([welf.delegate respondsToSelector:@selector(downloadDidCancelForObject:withError:)]) {
                                                                                [welf.delegate downloadDidCancelForObject:object withError:error];
                                                                            }
                                                                        }
                                                                     complete:^(BOOL downloadFinished, NSString *pathToFile) {
                                                                         object.state = DownloadableObjectDownloadFinished;
                                                                         welf.numberOfDownloadingItems--;
                                                                         welf.numberOfDownloadedItems++;
                                                                         [welf removeSuccessfullyDownloadedObject:pathToFile];
                                                                         if ([welf.delegate respondsToSelector:@selector(downloadStateDidChangeForObject:)]) {
                                                                             [welf.delegate downloadStateDidChangeForObject:object];
                                                                         }
                                                                     }];
        [self.downloadQueue addOperation:operation];
    }
    
}

- (void)cancelAlloperations {
    
    [self.downloadQueue cancelAllOperations];
    
}

@end
