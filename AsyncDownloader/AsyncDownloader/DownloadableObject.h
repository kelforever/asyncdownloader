//
//  DownloadableObject.h
//  
//
//  Created by Dmitry Mozyrchuk on 4/29/16.
//
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, DownloadableObjectState){
    DownloadableObjectDownloadPlanned = 0,
    DownloadableObjectDownloadStarted,
    DownloadableObjectDownloadFinished,
    DownloadableObjectDownloadCancelled
};

@interface DownloadableObject : NSObject

@property (nonatomic, assign) DownloadableObjectState state;
@property (nonatomic, strong) NSURL *downloadURL;
@property (nonatomic, strong) NSString *filePath;
@property (nonatomic, strong) NSString *fileName;
@property (nonatomic, assign) double totalFileLength;
@property (nonatomic, assign) double currentDownloadedLenght;

- (instancetype)initWithURL:(NSURL*)url
                   filePath:(NSString *)filePath
                   fileName:(NSString *)fileName;


@end
