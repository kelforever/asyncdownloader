//
//  DownloadManager.h
//  
//
//  Created by Dmitry Mozyrchuk on 4/29/16.
//
//

#import <Foundation/Foundation.h>
#import "DownloadableObject.h"

@protocol DownloadManagerDelegate <NSObject>

- (void)downloadStateDidChangeForObject:(DownloadableObject *)downloadableObject;
- (void)downloadDidCancelForObject:(DownloadableObject *)downloadableObject withError:(NSError *)error;

@end

@interface DownloadManager : NSObject

@property (nonatomic, strong) NSMutableArray *downloadableObjectsArray;
@property (nonatomic, strong) NSOperationQueue *downloadQueue;
@property (nonatomic, assign) NSInteger numberOfDownloadedItems;
@property (nonatomic, assign) NSInteger numberOfDownloadingItems;

@property (nonatomic, weak) id<DownloadManagerDelegate> delegate;

- (instancetype)initWithNumberofDownloadableItems:(NSInteger)totalDownloadsCount
                      numberOfConcurrentDownloads:(NSInteger)maxConcurrentDownloads
                                      downloadURL:(NSURL *)url
                                         filePath:(NSString *)filePath
                                         fileName:(NSString*)fileName
                                         delegate:(id<DownloadManagerDelegate>) delegate;

- (void)startDownloading;
- (void)cancelAlloperations;

@end
