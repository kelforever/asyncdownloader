//
//  SummaryViewController.h
//  
//
//  Created by Dmitry Mozyrchuk on 4/27/16.
//
//

#import <UIKit/UIKit.h>
#import "DownloadManager.h"

@interface SummaryViewController : UITableViewController<DownloadManagerDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableviewDownloadsSummary;
@property (nonatomic, strong) NSMutableArray *dataSource;
@property (nonatomic, strong) DownloadManager *downloadManager;

@end
