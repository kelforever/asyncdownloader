//
//  SummaryViewController.m
//  
//
//  Created by Dmitry Mozyrchuk on 4/27/16.
//
//

#import "SummaryViewController.h"
#import "SummaryCell.h"
#import "DownloadableObject.h"

#define kNumberOfDownloadableitem   100
#define kNumberOfConcurrentDownloads  8
#define kFileName   @"sample_file"
#define kDownloadURL @"https://www.dropbox.com/s/3o9ubszrwpt1hh6/1223.pdf?dl=1"


@interface SummaryViewController ()

- (NSString *)createDownloadFolder;
- (void)clearDownloadFolder;

@end

@implementation SummaryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = [NSString stringWithFormat:@"Downloaded - %d / Downloaing - %d / In Queue - %d", 0, 0, kNumberOfDownloadableitem];
    NSString *filePath = [NSString stringWithFormat:@"%@/%@", [self createDownloadFolder], kFileName];
    self.downloadManager = [[DownloadManager alloc] initWithNumberofDownloadableItems:kNumberOfDownloadableitem numberOfConcurrentDownloads:kNumberOfConcurrentDownloads downloadURL:[NSURL URLWithString:kDownloadURL] filePath:filePath fileName:kFileName delegate:self];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [self.downloadManager startDownloading];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    [self.downloadManager cancelAlloperations];
    // Dispose of any resources that can be recreated.
}

#pragma mark = Internal Methods

- (void)clearDownloadFolder {
    
    NSFileManager *fm = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/DownloadFolder"];
    NSError *error = nil;
    for (NSString *file in [fm contentsOfDirectoryAtPath:dataPath error:&error]) {
        BOOL success = [fm removeItemAtPath:[NSString stringWithFormat:@"%@/%@", dataPath, file] error:&error];
        if (!success || error) {
            NSLog(@"Error while trying to remove old downloads %@", error);
        }
    }
    
}

- (NSString *)createDownloadFolder {
    
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/DownloadFolder"];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
        if (!error) {
            return dataPath;
        } else {
            NSLog(@"Error creating download folder %@", error.localizedDescription);
            return nil;
        }
    } else {
        [self clearDownloadFolder];
        return dataPath;
    }
    
    
}

#pragma mark - DownloadManagerDelegate methods

- (void)downloadStateDidChangeForObject:(DownloadableObject *)downloadableObject {
    
    NSIndexPath *currentIndexPath = [NSIndexPath indexPathForRow:[self.downloadManager.downloadableObjectsArray indexOfObject:downloadableObject] inSection:0];
    __weak typeof(self) welf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [welf.tableviewDownloadsSummary reloadRowsAtIndexPaths:[NSArray arrayWithObject:currentIndexPath] withRowAnimation:UITableViewRowAnimationNone];
        welf.title = [NSString stringWithFormat:@"Downloaded - %ld / Downloading - %lu / In Queue - %lu", (long)welf.downloadManager.numberOfDownloadedItems, (long)welf.downloadManager.numberOfDownloadingItems, (long)(welf.downloadManager.downloadQueue.operationCount - welf.downloadManager.numberOfDownloadingItems)];
    });
    
}

- (void)downloadDidCancelForObject:(DownloadableObject *)downloadableObject withError:(NSError *)error {
    
    NSLog(@"Error while downloading file %@ \nerror:%@", downloadableObject.fileName, error.localizedDescription);
    NSIndexPath *currentIndexPath = [NSIndexPath indexPathForRow:[self.downloadManager.downloadableObjectsArray indexOfObject:downloadableObject] inSection:0];
    __weak typeof(self) welf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [welf.tableviewDownloadsSummary reloadRowsAtIndexPaths:[NSArray arrayWithObject:currentIndexPath] withRowAnimation:UITableViewRowAnimationNone];
        welf.title = [NSString stringWithFormat:@"Downloaded - %ld / Downloading - %lu / In Queue - %lu", (long)welf.downloadManager.numberOfDownloadedItems, (long)welf.downloadManager.numberOfDownloadingItems, (long)(welf.downloadManager.downloadQueue.operationCount - welf.downloadManager.numberOfDownloadingItems)];
    });
    
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return self.downloadManager.downloadableObjectsArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *simpleTableIdentifier = @"SummaryCell";
    
    SummaryCell *cell = (SummaryCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    DownloadableObject *currentObject = [self.downloadManager.downloadableObjectsArray objectAtIndex:indexPath.row];
    cell.labelSummary.text = currentObject.fileName;
    switch (currentObject.state) {
        case DownloadableObjectDownloadPlanned: {
            cell.labelDownloadStatus.hidden = NO;
            cell.viewDownloadProgress.hidden = YES;
            cell.labelDownloadStatus.text = @"In queue";
        }
            break;
        case DownloadableObjectDownloadStarted: {
            cell.labelDownloadStatus.hidden = YES;
            cell.viewDownloadProgress.hidden = NO;
            [cell.progressViewDownloadProgress setProgress:currentObject.currentDownloadedLenght / currentObject.totalFileLength animated:NO];
            cell.labelDownloadProgress.text = [NSString stringWithFormat:@"%0.1f of %0.1f Mb", currentObject.currentDownloadedLenght / 1000000, currentObject.totalFileLength / 1000000];
        }
            break;
        case DownloadableObjectDownloadFinished: {
            cell.labelDownloadStatus.hidden = NO;
            cell.viewDownloadProgress.hidden = YES;
            cell.labelDownloadStatus.text = @"Downloaded";
        }
            break;
        case DownloadableObjectDownloadCancelled: {
            cell.labelDownloadStatus.hidden = NO;
            cell.viewDownloadProgress.hidden = YES;
            cell.labelDownloadStatus.text = @"Cancelled";
        }
            break;
            
        default: {
            cell.labelDownloadStatus.hidden = NO;
            cell.viewDownloadProgress.hidden = YES;
            cell.labelDownloadStatus.text = @"In queue";
        }
            break;
    }
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
