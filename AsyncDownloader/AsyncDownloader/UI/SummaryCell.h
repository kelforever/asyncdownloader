//
//  SummaryCell.h
//  
//
//  Created by Dmitry Mozyrchuk on 4/27/16.
//
//

#import <UIKit/UIKit.h>

@interface SummaryCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *labelSummary;
@property (strong, nonatomic) IBOutlet UILabel *labelDownloadStatus;
@property (strong, nonatomic) IBOutlet UIProgressView *progressViewDownloadProgress;
@property (strong, nonatomic) IBOutlet UIView *viewDownloadProgress;
@property (strong, nonatomic) IBOutlet UILabel *labelDownloadProgress;
@end
