//
//  DownloadOperation.m
//  
//
//  Created by Dmitry Mozyrchuk on 4/29/16.
//
//

#import "DownloadOperation.h"

@interface DownloadOperation ()

// Download
@property (nonatomic, strong) NSURLConnection *connection;
@property (nonatomic, strong) NSMutableData *receivedDataBuffer;
@property (nonatomic, strong) NSFileHandle *file;
@property (nonatomic, assign) uint64_t expectedDataLength;
@property (nonatomic, assign) uint64_t receivedDataLength;
// Blocks
@property (nonatomic, copy) void (^firstResponseBlock)(NSURLResponse *response);
@property (nonatomic, copy) void (^progressBlock)(uint64_t receivedLength, uint64_t totalLength);
@property (nonatomic, copy) void (^errorBlock)(NSError *error);
@property (nonatomic, copy) void (^completeBlock)(BOOL downloadFinished, NSString *pathToFile);

+ (NSNumber *)freeDiskSpace;

- (void)finishOperationWithState:(DownloadState)state;
- (void)notifyFromCompletionWithError:(NSError *)error pathToFile:(NSString *)pathToFile;
- (BOOL)removeFileWithError:(NSError *__autoreleasing *)error;

@end


@implementation DownloadOperation

- (instancetype)initWithURL:(NSURL *)url
               downloadPath:(NSString *)pathToDL
              firstResponse:(void (^)(NSURLResponse *response))firstResponseBlock
                   progress:(void (^)(uint64_t receivedLength, uint64_t totalLength))progressBlock
                      error:(void (^)(NSError *error))errorBlock
                   complete:(void (^)(BOOL downloadFinished, NSString *pathToFile))completeBlock
{
    self = [super init];
    if (self) {
        self.downloadURL = url;
        self.pathToFile = pathToDL;
        self.state = DownloadStateReady;
        self.fileRequest = [NSMutableURLRequest requestWithURL:self.downloadURL
                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
                                               timeoutInterval:kDefaultRequestTimeout];
        self.firstResponseBlock = firstResponseBlock;
        self.progressBlock = progressBlock;
        self.errorBlock = errorBlock;
        self.completeBlock = completeBlock;
    }
    return self;
}

#pragma mark - NSOperation Override


- (void)start
{
    if (![NSThread isMainThread])
    {
        [self performSelectorOnMainThread:@selector(start) withObject:nil waitUntilDone:NO];
        return;
    }
    if ([self isCancelled]) {
        return;
    }
    
    if (![NSURLConnection canHandleRequest:self.fileRequest]) {
        NSError *error = [NSError errorWithDomain:NSURLErrorDomain
                                             code:NSURLErrorBadURL
                                         userInfo:@{ NSLocalizedDescriptionKey:
                                                         [NSString stringWithFormat:@"Invalid URL provided: %@", self.fileRequest.URL] }];
        
        [self notifyFromCompletionWithError:error pathToFile:nil];
        return;
    }
    
    NSFileManager *fm = [NSFileManager defaultManager];
    
    // Test if file already exists (partly downloaded) to set HTTP `bytes` header or not
    if (![fm fileExistsAtPath:self.pathToFile]) {
        [fm createFileAtPath:self.pathToFile
                    contents:nil
                  attributes:nil];
    }
    else {
        uint64_t fileSize = [[fm attributesOfItemAtPath:self.pathToFile error:nil] fileSize];
        NSString *range = [NSString stringWithFormat:@"bytes=%lld-", fileSize];
        [self.fileRequest setValue:range forHTTPHeaderField:@"Range"];
        // Allow progress to reflect what's already downloaded
        self.receivedDataLength += fileSize;
    }
    
    self.file = [NSFileHandle fileHandleForWritingAtPath:self.pathToFile];
    self.receivedDataBuffer = [[NSMutableData alloc] init];
    self.connection = [[NSURLConnection alloc] initWithRequest:self.fileRequest
                                                      delegate:self
                                              startImmediately:NO];
    
    if (self.connection && ![self isCancelled]) {
        [self willChangeValueForKey:@"isExecuting"];
        self.state = DownloadStateDownloading;
        [self didChangeValueForKey:@"isExecuting"];
        
        [self.file seekToEndOfFile];
        
        // Start the download
        NSRunLoop* runLoop = [NSRunLoop currentRunLoop];
        [self.connection scheduleInRunLoop:runLoop
                                   forMode:NSDefaultRunLoopMode];
        [self.connection start];
        
    }
}

- (BOOL)isExecuting
{
    return self.state == DownloadStateDownloading;
}

- (BOOL)isCancelled
{
    return self.state == DownloadStateCancelled;
}

- (BOOL)isFinished
{
    return self.state == DownloadStateCancelled || self.state == DownloadStateDone || self.state == DownloadStateFailed;
}

#pragma mark - NSURLConnection Delegate


- (void)connection:(NSURLConnection*)connection didFailWithError:(NSError *)error
{
    [self notifyFromCompletionWithError:error pathToFile:self.pathToFile];
}

- (void)connection:(NSURLConnection*)connection didReceiveResponse:(NSURLResponse *)response
{
    // If anything was previousy downloaded, add it to the total expected length for the progress property
    self.expectedDataLength = self.receivedDataLength + [response expectedContentLength];
    
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
    NSError *error;
    if (httpResponse.statusCode >= 400) {
        error = [NSError errorWithDomain:NSURLErrorDomain
                                    code:httpResponse.statusCode
                                userInfo:@{ NSLocalizedDescriptionKey:[NSString stringWithFormat:@"Erroneous HTTP status code %ld (%@)",
                                                                       (long) httpResponse.statusCode,
                                                                       [NSHTTPURLResponse localizedStringForStatusCode:httpResponse.statusCode]]}];
    }
    
    long long expected = @(self.expectedDataLength).longLongValue;
    if ([DownloadOperation freeDiskSpace].longLongValue < expected && expected != -1) {
        error = [NSError errorWithDomain:NSURLErrorDomain
                                    code:NSURLErrorCannotMoveFile
                                userInfo:@{ NSLocalizedDescriptionKey:@"Not enough free disk space" }];
    }
    
    if (!error) {
        [self.receivedDataBuffer setData:nil];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (self.firstResponseBlock) {
                self.firstResponseBlock(response);
            }
        });
    }
    else {
        [self notifyFromCompletionWithError:error pathToFile:self.pathToFile];
    }
}

- (void)connection:(NSURLConnection*)connection didReceiveData:(NSData *)data
{
    [self.receivedDataBuffer appendData:data];
    self.receivedDataLength += [data length];
    
    if (self.receivedDataBuffer.length > kBufferSize && [self isExecuting]) {
        [self.file writeData:self.receivedDataBuffer];
        [self.receivedDataBuffer setData:nil];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.progressBlock) {
            self.progressBlock(self.receivedDataLength, self.expectedDataLength);
        }
    });
}

- (void)connectionDidFinishLoading:(NSURLConnection*)connection
{
    if ([self isExecuting]) {
        [self.file writeData:self.receivedDataBuffer];
        [self.receivedDataBuffer setData:nil];
        
        [self notifyFromCompletionWithError:nil pathToFile:self.pathToFile];
    }
}

#pragma mark - Public Methods


- (void)cancelDownloadAndRemoveFile:(BOOL)remove
{
    // Cancel the connection before deleting the file
    [self.connection cancel];
    
    if (remove) {
        NSError *error;
        if (![self removeFileWithError:&error]) {
            [self notifyFromCompletionWithError:error pathToFile:nil];
            return;
        }
    }
    
    [self cancel];
}


#pragma mark - Internal Methods


- (void)finishOperationWithState:(DownloadState)state
{
    [self.connection cancel];
    [self.file closeFile];
    
    if ([self isExecuting]) {
        [self willChangeValueForKey:@"isFinished"];
        [self willChangeValueForKey:@"isExecuting"];
        self.state = state;
        [self didChangeValueForKey:@"isExecuting"];
        [self didChangeValueForKey:@"isFinished"];
    } else {
        [self willChangeValueForKey:@"isExecuting"];
        self.state = state;
        [self didChangeValueForKey:@"isExecuting"];
    }
}

- (void)cancel
{
    [self willChangeValueForKey:@"isCancelled"];
    [self finishOperationWithState:DownloadStateCancelled];
    [self didChangeValueForKey:@"isCancelled"];
}

- (void)notifyFromCompletionWithError:(NSError *)error pathToFile:(NSString *)pathToFile
{
    BOOL success = error == nil;
    
    if (error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (self.errorBlock) {
                self.errorBlock(error);
            }
        });
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.completeBlock) {
            self.completeBlock(success, pathToFile);
        }
    });
    
    DownloadState finalState = success ? DownloadStateDone : DownloadStateFailed;
    [self finishOperationWithState:finalState];
}

- (BOOL)removeFileWithError:(NSError *__autoreleasing *)error
{
    NSFileManager *fm = [NSFileManager defaultManager];
    if ([fm fileExistsAtPath:self.pathToFile]) {
        return [fm removeItemAtPath:self.pathToFile error:error];
    }
    
    return YES;
}

+ (NSNumber *)freeDiskSpace
{
    NSDictionary *fattributes = [[NSFileManager defaultManager] attributesOfFileSystemForPath:NSHomeDirectory() error:nil];
    return [fattributes objectForKey:NSFileSystemFreeSize];
}


@end
