//
//  main.m
//  AsyncDownloader
//
//  Created by Dmitry Mozyrchuk on 27.04.16.
//  Copyright (c) 2016 Dmitry Mozyrchuk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
