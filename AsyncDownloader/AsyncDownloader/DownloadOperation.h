//
//  DownloadOperation.h
//  
//
//  Created by Dmitry Mozyrchuk on 4/29/16.
//
//

#import <Foundation/Foundation.h>

static const double kBufferSize = 1000*1000; // 1 MB
static const NSTimeInterval kDefaultRequestTimeout = 30;

typedef NS_ENUM(NSUInteger, DownloadState) {
    DownloadStateReady = 0,
    DownloadStateDownloading,
    DownloadStateDone,
    DownloadStateCancelled,
    DownloadStateFailed
};

@interface DownloadOperation : NSOperation <NSURLConnectionDelegate>


@property (nonatomic, copy) NSURL *downloadURL;
@property (nonatomic, copy) NSString *pathToFile;
@property (nonatomic, strong) NSMutableURLRequest *fileRequest;
@property (nonatomic, assign) DownloadState state;

- (instancetype)initWithURL:(NSURL *)url
               downloadPath:(NSString *)pathToDL
              firstResponse:(void (^)(NSURLResponse *response))firstResponseBlock
                   progress:(void (^)(uint64_t receivedLength, uint64_t totalLength))progressBlock
                      error:(void (^)(NSError *error))errorBlock
                   complete:(void (^)(BOOL downloadFinished, NSString *pathToFile))completeBlock;

- (void)cancelDownloadAndRemoveFile:(BOOL)remove;


@end
